// Simple utility to convert a string using a key/value file

package main

import (
    "bufio"
    "flag"
    "fmt"
    "log"
    "os"
    "strings"
    "time"

    "bitbucket.org/GottySG36/utils"
)

var eq      = flag.String("e", "", "Csv file containing the string to be converted and its match")
var values  = flag.String("values", "", "List of values to lookup, comma seperated")
var inter   = flag.Bool("interact", false, "Will prompt users for the strings to convert, one at a time")
var rev     = flag.Bool("reverse", false, "Will use the second column for keys instead of the first")

func LoadEq(file string, reverse bool) (map[string]string, error) {
    conv := make(map[string]string)

    f, err := os.Open(file)
    defer f.Close()
    if err != nil {
        return nil, err
    }
    s := bufio.NewScanner(f)
    for s.Scan() {
        //skip empty lines
        if s.Text() == "" {
            continue
        }

        // Split string in three and keep only first two items
        val := strings.SplitN(s.Text(), ",", 3)[:2]
        if len(val) != 2 {
            log.Printf("Warning -:- Malformed line found (%v). Skipping line...\n", s.Text())
            continue
        }

        if reverse {
            conv[val[1]] = val[0]
        } else {
            conv[val[0]] = val[1]
        }
    }
    return conv, nil
}

func ConvertValue(value string, converter map[string]string) string {
    match := converter[value]
    if match == "" {
        return "Not Found!"
    }
    return match
}

func main() {
    flag.Parse()

    quit := make(chan struct{})
    go utils.Spinner("Loading Equivalence file", 100 * time.Millisecond, quit)
    conv, err := LoadEq(*eq, *rev)
    if err != nil {
        log.Fatalf("Error -:- LoadEq : %v\n", err)
    }
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf(" Done!\n\n")

    if *values != "" {
        for _, s := range strings.Split(*values, ",") {
            match := ConvertValue(s, conv)
            fmt.Printf("Query %v : %v\n", s, match)
        }
    }

    var exit bool
    if *inter {
        for {
            if exit {
                break
            }
            query := utils.GetInput("String to search? ")
            switch query {
            case "":
                fmt.Printf("Empty query, please enter a string to search ('exit' to leave)\n")
            case "exit":
                fmt.Printf("Now leaving...\n")
                exit = true
                break
            default:
                match := ConvertValue(query, conv)
                fmt.Printf("Query %v : %v\n", query, match)
            }
        }
    }


}
